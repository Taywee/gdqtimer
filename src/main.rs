use gdqtimer::{Braille, DecimalNibble};
use std::io::{self, Write};
use std::thread::sleep;
use std::time::{Duration, Instant};

fn main() -> Result<(), Box<dyn std::error::Error>>{
    let mut deciseconds = 0u8;
    let mut seconds = 0u8;
    let mut minutes = 0u8;
    let mut hours = 0u8;

    let decisecond = Duration::from_millis(100);

    let mut next_tick = Instant::now();
    let mut stdout = io::stdout();

    loop {
        next_tick = next_tick.checked_add(decisecond).unwrap();

        // Drop to the beginning of the line and print out the decimal clock, not printing too many
        // digits, but keeping the scale aligned (at least until we hit 3 digits for hours)
        print!("\r");
        if hours > 0 {
            print!("{:02}:{:02}:{:02}.{}", hours, minutes, seconds, deciseconds);
        } else if minutes > 0 {
            print!("   {:02}:{:02}.{}", minutes, seconds, deciseconds);
        } else if seconds > 9 {
            print!("      {:02}.{}", seconds, deciseconds);
        } else {
            print!("       {}.{}", seconds, deciseconds);
        }

        // Get the binary-coded decimal digits
        let hour_byte = hours.decimal_nibble();
        let minute_byte = minutes.decimal_nibble();
        let second_byte = seconds.decimal_nibble();

        // Print the binary-coded decimal digits, shifting them because we're doing deciseconds
        print!(
            " {}{}{}{}",
            (hour_byte >> 4).ordered_braille(),
            (hour_byte << 4 | minute_byte >> 4).ordered_braille(),
            (minute_byte << 4 | second_byte >> 4).ordered_braille(),
            (second_byte << 4 | deciseconds.decimal_nibble()).ordered_braille()
        );
        stdout.flush()?;

        // We could do this with chrono or build a type specifically for this kind of thing
        deciseconds += 1;
        if deciseconds >= 10 {
            deciseconds = 0;
            seconds += 1;
            if seconds >= 60 {
                seconds = 0;
                minutes += 1;
                if minutes >= 60 {
                    minutes = 0;
                    hours += 1;
                }
            }
        }

        // Sleep if the next tick is in the future.
        if let Some(duration) = next_tick.checked_duration_since(Instant::now()) {
            sleep(duration);
        }
    }
}

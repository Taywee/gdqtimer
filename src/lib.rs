use std::convert::TryInto;

/// A trait to get braille characters.
pub trait Braille: Copy {
    /// Get braille directly, in the Unicode ordering
    fn braille(self) -> char;

    /// Get braille with bits ordered from least significant to most significant bottom-to-top,
    /// right-to-left.
    fn ordered_braille(self) -> char;
}

impl Braille for u8 {
    fn braille(self) -> char {
        // U+2800 is BRAILLE PATTERN EMPTY
        // We use this instead of '⠀' as u32 because that just looks like a space, so this is clear
        // and unambiguous.
        (0x2800u32 + self as u32).try_into().unwrap()
    }

    fn ordered_braille(self) -> char {
        let mut reordered = 0u8;
        // 1 -> 8
        reordered |= (self & 0b00000001) << 7;
        // 2 -> 6
        reordered |= (self & 0b00000010) << 4;
        // 3 -> 5
        reordered |= (self & 0b00000100) << 2;
        // 4 -> 4
        reordered |= self & 0b00001000;
        // 5 -> 7
        reordered |= (self & 0b00010000) << 2;
        // 6 -> 3
        reordered |= (self & 0b00100000) >> 3;
        // 7 -> 2
        reordered |= (self & 0b01000000) >> 5;
        // 8 -> 1
        reordered |= (self & 0b10000000) >> 7;
        reordered.braille()
    }
}

/// Split decimal digits to nibbles.
///
/// Effectively, this is binary-coded decimal.
///
/// Doesn't work properly above 2 digits.  We could panic if we cared, but we don't.
pub trait DecimalNibble: Copy {
    fn decimal_nibble(self) -> u8;
}

impl DecimalNibble for u8 {
    fn decimal_nibble(self) -> u8 {
        self / 10 << 4 | self % 10
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn braille() {
        assert_eq!(0x13u8.braille(), '⠓');
        assert_eq!(0x13u8.ordered_braille(), '⣠');

        assert_eq!(0xC7u8.braille(), '⣇');
        assert_eq!(0xC7u8.ordered_braille(), '⢳');

        assert_eq!(0xFFu8.braille(), '⣿');
        assert_eq!(0xFFu8.ordered_braille(), '⣿');
    }

    #[test]
    fn decimal_nibble() {
        assert_eq!(5.decimal_nibble(), 0x05);
        assert_eq!(10.decimal_nibble(), 0x10);
        assert_eq!(23.decimal_nibble(), 0x23);
        assert_eq!(123.decimal_nibble(), 0xC3);
    }
}

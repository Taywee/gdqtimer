# gdqtimer

This is a simple implementation of the AGDQ timer underlay (visible in [this
run](https://www.youtube.com/watch?v=W1DVfujDbCI), for example), which is a
binary-coded decimal clock.  Because this only needs a column of 4 bits per
digit, we can use the Unicode braille block to represent this.

```
13:23.5 ⠀⢀⡤⣔
20:13.6 ⠀⠠⢀⡴
23:01.9 ⠀⠠⡄⣈
27:25.3 ⠀⠠⡦⣢
31:20.3 ⠀⢠⡠⢠
34:20.3 ⠀⢠⠢⢠
```

# running

You know the drill

```
cargo run --release
```

This has no external dependencies.

# COPYRIGHT and LICENSE

This code is Copyright 2021 Taylor C. Richberger, and is licensed under the GNU
GPL version 3.0 or later.
